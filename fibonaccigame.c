/* fibonaccigame on C */
#include <stdio.h>

long fibonacci(int n)
{
    if(n == 0 || n == 1) {
        return n;
    }

    int p, q, r;
    p = q = 0;
    r = 1;
    for(int i = 2; i <= n; i++) {
        p = q;
        q = r;
        r = p + q;
    }
    return r;
}

void show_sequence(int n)
{
    for(int i = 0; i <= n; i++) {
        printf("%ld\n", fibonacci(i));
    }
}

int main()
{
    int n;
    long prev, cur, ans, rightans;
    n = 1;
    cur = 0;
    for(;;) {
        prev = cur;
        cur = fibonacci(n);
        rightans = prev + cur;
        printf("%ld %ld ?\nYour answer: ", prev, cur);
        scanf("%ld", &ans);
        if(ans != rightans) {
            printf("Wrong, right answer is %ld\n", rightans);
            show_sequence(n);
            return 0;
        }
        n++;
    }
}
